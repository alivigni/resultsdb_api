# ResultsDB library

The ResultsDB API module provides a Python API for using ResultsDB's JSON/REST interface in a more pythonic way. It has functions which match the JSON/REST methods, but allow the common goodies as named parameters, and parameters skipping.

# License

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

# Repositories

* ResultsDB - [Bitbucket GIT repo](https://bitbucket.org/fedoraqa/resultsdb)
* ResultsDB Frontend - [Bitbucket GIT repo](https://bitbucket.org/fedoraqa/resultsdb_frontend)
